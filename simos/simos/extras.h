#pragma once

int rand_func(int, char**);
int freq(int, char**);
int type(int, char**);
int wc(int, char**);
int sort(int, char**);
int echo(int, char**);
int _strcmp(const void*, const void*);