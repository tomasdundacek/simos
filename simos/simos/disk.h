#pragma once
#include "list.h"

typedef struct file {
	char* name;
	enum { DISK_FILE = 1, DISK_DIRECTORY } fileType;
	struct file* pParent;
	size_t size;
	tList* items;
	char* content;
} tFile;

int initDisk();
int freeDisk();
int cd(int, char**);
int md(int, char**);
int dir(int, char**);
int rd(int, char**);

tFile* _createDirectory(char*);
tFile* _searchFile(char*, short);
tFile* _searchFileDeep(char*, short);
tFile* _createFile(char*);
int _isDirectoryEmpty();