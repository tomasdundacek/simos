#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include "shell.h"
#include "disk.h"
#include "runtime.h"

extern tFile* pCurrentDirectory;

int shell(int argc, char* args[]) {
	if (argc > 0) {
		printf("Shell program does not accept any arguments\n");
		return 1;
	}

	printf("Welcome to shell!\n");

	while (1) {
		char* command = malloc(sizeof(char) * 1024);
		tProcess** commands = (tProcess**) malloc(sizeof(tProcess**));
		int commandsCount;
		HANDLE* aThread;
		DWORD ThreadId;

		printf("$ %s> ", pCurrentDirectory->name);
		gets_s(command, 1024);

		commandsCount = _parseCommands(&commands, command);

		if (strcmp(commands[0]->programName, "exit") == 0) {
			break;
		}

		aThread = malloc(sizeof(HANDLE) * commandsCount);

		for (int i = 0; i < commandsCount; i++) {
			aThread[i] = CreateThread(NULL, 0, callProgram, commands[i], 0, &ThreadId);

			if (aThread[i] == NULL) {
				printf("Thread creation failed: %s", GetLastError());
				return 1;
			}

			WaitForMultipleObjects(commandsCount, aThread, TRUE, INFINITE);
		}
	}
	return 0;
}

int _parseCommands(tProcess*** outputAddress, char command[]) {
	int commandsCount = 0;
	char* token = NULL;
	char* args = NULL;
	char* commandBuffer = NULL;
	char* commandTokenBuffer = NULL;
	char* tokenBuffer = NULL;
	char* commandString = malloc((strlen(command) + 1) * sizeof(char));
	short hasArguments;
	
	tProcess** output = *outputAddress;

	strcpy_s(commandString, strlen(command) + 1, command);

	for (commandBuffer = strtok_s(commandString, "|", &commandTokenBuffer); commandBuffer != NULL; commandBuffer = strtok_s(NULL, "|", &commandTokenBuffer)) {
		output = realloc(output, sizeof(tProcess**) * (commandsCount + 1));
		token = strtok_s(commandBuffer, " ", &tokenBuffer);

		output[commandsCount] = malloc(sizeof(tProcess));
		output[commandsCount]->programName = malloc((strlen(token) + 1) * sizeof(char));
		strcpy_s(output[commandsCount]->programName, strlen(token) + 1, token);

		token = strtok_s(NULL, "", &tokenBuffer);

		_parseOutputRedirection(&output[commandsCount], token);
		_parseInputRedirection(&output[commandsCount], token);

		// rewrite to "PID"
		output[commandsCount]->pipedIn = output[commandsCount]->pipedOut = 0;
		if(commandsCount > 0)
			output[commandsCount]->pipedIn = output[commandsCount - 1]->pipedOut = 1;

		hasArguments = token && (token[0] == '<' || token[0] == '>') ? 0 : 1;
		args = hasArguments ? strtok_s(token, "<>", &tokenBuffer) : NULL;

		output[commandsCount]->arguments = args ? _strdup(args) : NULL;
		
		commandsCount++;
	}

	*outputAddress = output;
	return commandsCount;
}

void _parseOutputRedirection(tProcess** process, char* args) {
	char* tmp;
	char* token = NULL;
	char* token2 = NULL;
	char* tokenBuffer = NULL;
	tProcess* output = *process;
	output->outputRedirectionFileName = NULL;

	if (args) {
		tmp = malloc(strlen(args) * sizeof(char) + 1);
		strcpy_s(tmp, strlen(args) + 1, args);

		token = strstr(tmp, ">>");
		output->isOutputRedirectionAppend = token ? 1 : 0;

		if (!token)
			token = strstr(tmp, ">");

		if (token) {
			token += (1 + output->isOutputRedirectionAppend) * sizeof(char);
			token2 = strtok_s(token, " ", &tokenBuffer);

			output->outputRedirectionFileName = _strdup(token2);
		}
	}
}

void _parseInputRedirection(tProcess** process, char* args) {
	char* tmp;
	char* token = NULL;
	char* token2 = NULL;
	char* tokenBuffer = NULL;
	tProcess* output = *process;
	output->inputRedirectionFileName = NULL;

	if (args) {
		tmp = malloc(strlen(args) * sizeof(char) + 1);
		strcpy_s(tmp, strlen(args) + 1, args);

		token = strstr(tmp, "<");
		if (token) {
			token += sizeof(char);
			token2 = strtok_s(token, " ", &tokenBuffer);

			output->inputRedirectionFileName = _strdup(token2);
		}
	}
}