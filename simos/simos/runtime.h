#pragma once
#include "shell.h"

typedef struct {
	char* name;
	int(*pProg)(int, char**);
} tProg;

int callProgram(tProcess*);