#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <fcntl.h>
#include <io.h>
#include <tchar.h>
#include "runtime.h"
#include "list.h"
#include "disk.h"

extern tProg programs[];
extern int programsCount;
extern tList* processes;


int callProgram(tProcess* program) {
	char* argumentsString = NULL;
	char** argumentsArray = NULL;
	char* token = NULL;
	short argumentCount = 0;
	char* tokenBuffer = NULL;

	if (program->inputRedirectionFileName) {
		tFile* inputFile = _searchFileDeep(program->inputRedirectionFileName, DISK_FILE);
		if (inputFile) {
			program->arguments = malloc((inputFile->size + 1) * sizeof(char));
			strcpy_s(program->arguments, inputFile->size + 1, inputFile->content);
		}	
	}

	if (program->arguments) {
		argumentsString = malloc((strlen(program->arguments) + 1) * sizeof(char));
		// copy arguments string, as arguments variable is in protected memory
		strcpy_s(argumentsString, strlen(program->arguments) + 1, program->arguments);
	}
	else {
		argumentsString = malloc(sizeof(char));
		argumentsString = "";
	}

	for (token = strtok_s(argumentsString, " ", &tokenBuffer); token != NULL; token = strtok_s(NULL, " ", &tokenBuffer)) {
		argumentsArray = (char**)realloc(argumentsArray, (argumentCount + 1) * sizeof(char*));
		
		argumentsArray[argumentCount] = malloc(strlen(token) + 1);
		strcpy_s(argumentsArray[argumentCount], strlen(token) + 1, token);

		argumentCount++;
	}

	for (short i = 0; i < programsCount; i++) {
		if (strcmp(programs[i].name, program->programName) == 0) {
			char buffer[1025] = { 0 };
			int returnValue = 1;
			int stdoutFileno;

			if (program->outputRedirectionFileName) {
				fflush(stdout);
				stdoutFileno = _dup(1);
				freopen("NUL", "a", stdout);
				setvbuf(stdout, buffer, _IOFBF, 1024);
			}

			returnValue = programs[i].pProg(argumentCount, argumentsArray);

			if (program->outputRedirectionFileName) {
				tFile* outputFile = _createFile(program->outputRedirectionFileName, DISK_FILE);
				outputFile->content = malloc(strlen(buffer) + 1);
				strcpy_s(outputFile->content, strlen(buffer) + 1, buffer);
				outputFile->size = strlen(buffer);

				freopen("NUL", "a", stdout);
				_dup2(stdoutFileno, 1);
				setvbuf(stdout, NULL, _IONBF, 0);
			}
			
			return returnValue;
		}
	}
	printf("Program %s not found\n", program->programName);

	return 1;
}