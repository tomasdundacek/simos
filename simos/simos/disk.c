#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "disk.h"
#include "list.h"

DISK_SIZE = 4 * 1024 * 1024; // Create a 4MB disk
char* pDisk = NULL;
tFile* pCurrentDirectory = NULL;
tFile* pRootDirectory = NULL;

int initDisk() {
	if (pDisk = calloc(1, DISK_SIZE)) {
		pCurrentDirectory = pRootDirectory = _createDirectory("C");
		return 1;
	}

	return 0;
}

int freeDisk() {
	free(pDisk);
	return 0;
}

int md(int argc, char* args[]) {
	tFile* _tmp = pCurrentDirectory;

	for (int i = 0; i < argc; i++) {
		char* dirName = malloc((strlen(args[i]) + 1) * sizeof(char));
		char* tokenBuffer = NULL;
		char* token = NULL;

		strcpy_s(dirName, strlen(args[i])+1, args[i]);

		for (token = strtok_s(dirName, "\\", &tokenBuffer); token != NULL; token = strtok_s(NULL, "\\", &tokenBuffer)) {
			pCurrentDirectory = _createDirectory(token);
		}

		pCurrentDirectory = _tmp;
	}

	return 0;
}

int cd(int argc, char** args) {
	tFile* _tmp = pCurrentDirectory;
	char* directoryString = malloc((strlen(args[0]) + 1) * sizeof(char));
	char* token = NULL;
	char* tokenBuffer = NULL;

	strcpy_s(directoryString, strlen(args[0]) + 1, args[0]);

	pCurrentDirectory = _searchFileDeep(directoryString, DISK_DIRECTORY);

	if (!pCurrentDirectory) {
		printf("Directory %s not found \n", args[0]);
		pCurrentDirectory = _tmp;
		return 1;
	}

	return 0;
}

int rd(int argc, char** args) {
	tFile* _tmp = pCurrentDirectory, *directoryFound = NULL;
	char** directories = NULL;
	int directoriesLength = 0;

	for (short i = 0; i < argc; i++) {
		directories = (char**)realloc(directories, (directoriesLength + 1) * sizeof(char*));
		directories[directoriesLength] = malloc((strlen(args[i]) + 1) * sizeof(char));
		strcpy_s(directories[directoriesLength], strlen(args[i]) + 1, args[i]);
		directoriesLength++;
	}

	for (short i = 0; i < directoriesLength; i++) {
		char* tokenBuffer = NULL;
		char* token = NULL;
		short found = 1;

		for (token = strtok_s(directories[i], "\\", &tokenBuffer); token != NULL; token = strtok_s(NULL, "\\", &tokenBuffer)) {
			pCurrentDirectory = directoryFound = _searchFileDeep(token, DISK_DIRECTORY);
			if (!pCurrentDirectory) {
				printf("Directory %s not found\n", args[i]);
				found = 0;
				break;
			}
		}

		if (found) {
			pCurrentDirectory = pCurrentDirectory->pParent;
			_searchFile(directoryFound->name, DISK_DIRECTORY);
			removeItem(pCurrentDirectory->items);
		}
		
		pCurrentDirectory = _tmp;
	}

	return 0;
}

int dir(int argc, char** args) {
	tFile* file = getFirst(pCurrentDirectory->items);
	tFile* _tmp = pCurrentDirectory;

	if (argc > 0) {
		char* dirName = malloc((strlen(args[0]) + 1) * sizeof(char));
		strcpy_s(dirName, strlen(args[0]) + 1, args[0]);
	
		pCurrentDirectory = _searchFileDeep(dirName, DISK_DIRECTORY);
		if (!pCurrentDirectory) {
			printf("Directory %s not found.\n", dirName);
			pCurrentDirectory = _tmp;
			return 1;
		}
	}

	do {
		if (file == pCurrentDirectory)
			printf(".\n");
		else if (file == pCurrentDirectory->pParent)
			printf("..\n");
		else {
			printf("%s\n", file->name);
			//if (file->fileType == DISK_FILE)
			//	printf("%d: %s\n", file->size, file->content);
		}
	} while (file = getNext(pCurrentDirectory->items));

	pCurrentDirectory = _tmp;

	return 0;
}

// Internal functions

// Creates a new directory or returns existing one
tFile* _createDirectory(char* name) {
	tFile *directory = _searchFile(name, DISK_DIRECTORY);

	if (directory) 
		return directory;

	directory = (tFile*)malloc(sizeof(tFile));

	directory->name = malloc(strlen(name));
	strncpy_s(directory->name, strlen(directory->name), name, strlen(name));

	directory->size = 0;
	directory->fileType = DISK_DIRECTORY;
	directory->pParent = pCurrentDirectory;
	directory->items = createList();

	insertHead(directory->items, directory);
	if (directory->pParent) {
		insertTail(directory->items, directory->pParent);
		insertTail(directory->pParent->items, directory);
	}

	return directory;
}

tFile* _createFile(char* name) {
	tFile *file = _searchFile(name, DISK_FILE);

	if (file)
		return file;

	file = (tFile*)malloc(sizeof(tFile));

	file->name = malloc(strlen(name));
	strncpy_s(file->name, strlen(file->name), name, strlen(name));

	file->size = 0;
	file->fileType = DISK_FILE;
	file->pParent = pCurrentDirectory;

	insertTail(file->pParent->items, file);

	return file;
}

tFile* _searchFile(char* name, short fileType) {
	if (!pCurrentDirectory) return NULL;

	tFile* tmp = getFirst(pCurrentDirectory->items);

	do {
		if (strcmp(tmp->name, name) == 0) {
			if (!fileType || fileType == tmp->fileType)
				return tmp;
		}
	} while (tmp = getNext(pCurrentDirectory->items));

	return NULL;
}

tFile* _searchFileDeep(char* name, short fileType) {
	char* token = NULL;
	char* tokenBuffer = NULL;
	char* fileName;
	tFile* _tmp = pCurrentDirectory;
	tFile* found = NULL;
	
	fileName = _strdup(name);

	if (!pCurrentDirectory) return NULL;

	for (token = strtok_s(fileName, "\\", &tokenBuffer); token != NULL; token = strtok_s(NULL, "\\", &tokenBuffer)) {
		if (strcmp(token, ".") == 0) continue;
		if (strcmp(token, "..") == 0) {
			pCurrentDirectory = pCurrentDirectory->pParent;
			continue;
		}

		pCurrentDirectory = _searchFile(token, 0);
		if (!pCurrentDirectory) break;
	}

	if (pCurrentDirectory && (fileType == 0 || pCurrentDirectory->fileType == fileType))
		found = pCurrentDirectory;
	
	pCurrentDirectory = _tmp;
	return found;
}

int _isDirectoryEmpty() {
	return (countItems(pCurrentDirectory->items) > 2) ? 0 : 1;
}