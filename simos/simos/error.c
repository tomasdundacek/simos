#include <stdio.h>
#include "error.h"

void error(int code) {
	printf("Error %d:", code);
	switch(code){
	case ERR_DISK_INIT:
		printf("disk initialization failed");
	}
	printf("\n");
}