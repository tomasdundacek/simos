/*
Name: Tomas Dundacek
Studiengruppe: 09/041/61
MatrNr.: 27997
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

/**
Creates a new List
@return Pointer of the new list | NULL(-pointer)
**/
tList* createList(void) {
	tList* pList;
	pList = malloc(sizeof(tList));
	if (pList) {
		pList->pFirst = pList->pLast = pList->pCurr = NULL;
	}

	return pList;
}

/**
Deletes an empty list
@param	tList* 	pList			Pointer of the list
@return int			1 on success | 0 on failure
**/

int deleteList(tList* pList) {

	free(pList);

	return 1;
}

/**
Inserts element behind the current element
@param	tList*	pList			Pointer of the list
@param	void*		pItem			The new item inserted
@return	int			1 on success | 0 on failure
**/

int insertBehind(tList* pList, void* pItem) {
	tCnct* ptmp;
	ptmp = malloc(sizeof(tCnct));

	if (ptmp) {
		ptmp->pPrv = pList->pCurr;
		ptmp->pNxt = pList->pCurr->pNxt;
		ptmp->pItem = pItem;

		ptmp->pPrv->pNxt = ptmp;
		ptmp->pNxt->pPrv = ptmp;
	}

	return ptmp ? 1 : 0;
}

/**
Inserts element before the current element
@param 	tList* 	pList			Pointer of the list
@param	void*		pItem			The new item inserted
@return int			1 on success | 0 on failure
**/

int insertBefore(tList* pList, void* pItem) {
	tCnct* ptmp;
	ptmp = malloc(sizeof(tCnct));

	if (ptmp) {
		ptmp->pPrv = pList->pCurr->pPrv;
		ptmp->pNxt = pList->pCurr;
		ptmp->pItem = pItem;

		ptmp->pPrv->pNxt = ptmp;
		ptmp->pNxt->pPrv = ptmp;
	}

	return ptmp ? 1 : 0;
}

/**
Inserts first element of a list
@param  tList* 	pList 		Pointer of the list
@param  void*  	pItem			The new item inserted
@return int		 	1 on success | 0 on failure
**/

int insertHead(tList* pList, void* pItem) {
	tCnct* ptmp;
	ptmp = malloc(sizeof(tCnct));
	if (ptmp) {
		ptmp->pItem = pItem;
		ptmp->pPrv = NULL;
		ptmp->pNxt = pList->pFirst;
		pList->pFirst = ptmp;

		if (ptmp->pNxt)
			ptmp->pNxt->pPrv = ptmp;
		else
			pList->pLast = ptmp;

		pList->pCurr = ptmp;
	}
	return ptmp ? 1 : 0;
}

/**
Inserts the element to the end of the list
@param	tList* 	pList	Pointer of the list
@param	void*		pItem	The new item inserted
@return int 		1 on success | 0 on failure
**/

int insertTail(tList* pList, void* pItem) {
	tCnct *ptmp;
	ptmp = malloc(sizeof(tCnct));
	if (ptmp) {
		ptmp->pPrv = pList->pLast;
		if (ptmp->pPrv)
			ptmp->pPrv->pNxt = ptmp;
		else
			pList->pFirst = ptmp;

		ptmp->pNxt = NULL;

		ptmp->pItem = pItem;
		pList->pLast = ptmp;
	}

	return ptmp ? 1 : 0;
}

int addItemToList(tList* pList, void *pItem, int(*fcmp)(void* pItList, void* pItNew)) {
	if (fcmp(pList, pItem))
		return 1;
	else
		return 0;
}

/**
Removes the current element of pList
@param	tList*	pList	Pointer of the list
@return	void
**/
void removeItem(tList *pList) {
	tCnct *ptmp = pList->pCurr;

	if (ptmp) {
		// If no next element
		if (ptmp->pNxt == NULL)
			pList->pLast = ptmp->pPrv;
		else
			ptmp->pNxt->pPrv = ptmp->pPrv;

		if (ptmp->pPrv == NULL)
			pList->pFirst = ptmp->pNxt;
		else
			ptmp->pPrv->pNxt = ptmp->pNxt;

		free(ptmp);
		pList->pCurr = NULL;
	}
}

/**
Gets the current element of the list
@param 	tList* 	pList	Pointer of the list
@return void*	Address of the selected item | NULL on failure
**/

void* getSelected(tList* pList) {
	tCnct *ptmp = pList->pCurr;
	if (ptmp) {
		pList->pCurr = ptmp;
		return ptmp->pItem;
	}
	else {
		return NULL;
	}
}

/**
Gets the first element of the list and selects it
@param tList* pList	Pointer of the list object
@return	void* Address of the first item | NULL on failure
**/
void* getFirst(tList*pList) {
	tCnct *ptmp = pList->pFirst;
	if (ptmp) {
		pList->pCurr = ptmp;
		return ptmp->pItem;
	}
	else
		return NULL;
}

/**
Gets the last element of the list and selects it
@param tList* pList	Pointer of the list
@return void* Address of the last item | NULL on failure
**/

void* getLast(tList* pList) {
	tCnct *ptmp = pList->pLast;
	if (ptmp) {
		pList->pCurr = ptmp;
		return ptmp->pItem;
	}
	else
		return NULL;
}

/**
Gets the next element in the list and selects it
@param 	tList* 	pList 	Pointer of the list
@return void*		Address of the next item | NULL on failure
**/

void* getNext(tList* pList) {
	tCnct *ptmp = pList->pCurr->pNxt;
	if (ptmp) {
		pList->pCurr = ptmp;
		return ptmp->pItem;
	}
	else
		return NULL;
}

/**
Gets the previous element in the list and selects it
@param 	tList*	pList	Pointer of the list
@return void*		Address of the last item | NULL on failure
**/

void* getPrev(tList* pList) {
	tCnct *ptmp = pList->pCurr->pPrv;
	if (ptmp) {
		pList->pCurr = ptmp;
		return ptmp->pItem;
	}
	else
		return NULL;
}

/**
Gets the element by its index starting from 0 and selects it
@param	tList*	pList	Pointer of the list
@param	int	Idx	index of the item to select
@return	void*		Address of the indexed item | NULL on failure
**/

void* getIndexed(tList* pList, int Idx) {
	int i; // Iterative var

	pList->pCurr = pList->pFirst;

	for (i = 0; i<Idx; i++) {
		if (!pList->pCurr->pNxt) return NULL;
		pList->pCurr = pList->pCurr->pNxt;
	}

	return pList->pCurr ? pList->pCurr->pItem : NULL;
}

/**
Counts items of the list given
@param	tList* pList	Pointer to the list
@return	int		 number of elements
*/
int countItems(tList* pList) {
	int i = 1;

	if (pList->pFirst == NULL) {
		return 0;
	}
	pList->pCurr = pList->pFirst;

	while (pList->pCurr != pList->pLast) {
		getNext(pList);
		i++;
	}

	return i;
}