#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <windows.h>
#include "runtime.h"
#include "disk.h"
#include "extras.h"

#define STOP 27

extern tProg programs[];
extern int programsCount;

int rand_func(int argc, char* argv[]) {
	do {
		printf("%f\n", (float) rand() / RAND_MAX);	
	} while (!(_kbhit() && (_getch() == STOP))); // for now

	return 0;
}

int type(int argc, char* argv[]) {
	tFile* fileFound = NULL;

	if (argc != 1) {
		printf("Type program requires exactly one argument.\n");
		return 1;
	}

	for (short i = 0; i < programsCount; i++) {
		if (strcmp(argv[0], programs[i].name) == 0) {
			printf("%s is a program\n", argv[0]);
			return 0;
		}
	}

	if ((fileFound = _searchFileDeep(argv[0], 0))) {
		if (fileFound->fileType == DISK_DIRECTORY)
			printf("%s is a directory\n", argv[0]);
		else
			printf("%s is a file\n", argv[0]);

		return 0;
	}

	printf("%s is not a program, a directory or a file\n", argv[0]);
	return 1;
}

int wc(int argc, char* argv[]) {
	int counterCache = 0, inword = 0;
	const char sentence[65535] = { '\0' };
	char* pSentence = sentence;

	if (argc == 0) {
		printf("wc needs arguments");
		return 1;
	}

	for (int i = 0; i < argc; i++) {
		strcat_s(sentence, strlen(sentence) + 1, argv[i]);
		strcat_s(sentence, 2 * sizeof(char), " ");
	}

	do switch (*pSentence) {
		case '\0':
		case ' ':
		case '\t':
		case '\n':
		case '\r':
			if (inword) { inword = 0; counterCache++; }
			break;
		default:
			inword = 1;
	} while (*pSentence++);

	printf("%d\n", counterCache);
	return 0;
}

int echo(int argc, char* argv[]) {
	if (argc == 0) {
		printf("No arguments found");
		return 1;
	}

	for (short i = 0; i < argc; i++) {
		printf("%s ", argv[i]);
	}

	printf("\n");

	return 0;
}

int freq(int argc, char* argv[]) {
	int chars[255] = { 0 };

	if (argc == 0) {
		printf("No arguments found");
		return 1;
	}

	for (short i = 0; i < argc; i++) {
		char* word = argv[i];

		do {
			if (*word <= 255) chars[*word]++;
		} while (*word++);
	}

	for (short i = 0; i < 255; i++) {
		if (chars[i] > 0)
			printf("0x%02X: %d\n", i, chars[i]);
	}
}

int sort(int argc, char* argv[]) {
	char** words = malloc(argc * sizeof(char*));
	char* buffer = NULL;

	if (argc == 0) {
		printf("Arguments missing");
		return 1;
	}

	for (int i = 0; i < argc; i++)
		words[i] = _strdup(argv[i]);

	qsort(words, argc, sizeof(char*), _strcmp);

	for (int i = 0; i < argc; i++) {
		printf("%s\n", words[i]);
	}

	return 0;
}

int _strcmp(const void* a, const void* b) {
	char const **char_a = a;
	char const **char_b = b;

	return strcmp(*char_a, *char_b);
}