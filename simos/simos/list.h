#pragma once

typedef struct tConnect {
	struct tConnect *pNxt;
	struct tConnect *pPrv;
	void * pItem;
}tCnct;

typedef struct {
	tCnct *pFirst;
	tCnct *pLast;
	tCnct *pCurr;
}tList;

// Functions for whole list
tList * createList(void);
int deleteList(tList* pList);

// Data-Management functions
int insertBehind(tList* pList, void *pItemIns);
int insertBefore(tList* pList, void *pItemIns);
int insertHead(tList* pList, void *pItemIns);
int insertTail(tList* pList, void *pItemIns);
int addItemToList(tList* pList, void *pItem, int(*fcmp)(void* pItList, void* pItNew));
void removeItem(tList *pList);

void* getSelected(tList* pList);
void* getFirst(tList* pList);
void* getLast(tList* pList);
void* getNext(tList* pList);
void* getPrev(tList* pList);
void* getIndexed(tList* pList, int Idx);

// Fkt. zum Berechnen der DS
int countItems(tList* pList);
