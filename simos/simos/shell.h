#pragma once

typedef struct {
	char* programName;
	char* arguments;
	short pipedIn;
	short pipedOut;
	char* outputRedirectionFileName;
	short isOutputRedirectionAppend;
	char* inputRedirectionFileName;
	enum { RUNNING = 1, WAITING } state;
} tProcess;

int shell(int argc, char* args[]);
int _parseCommands(tProcess*** output, char command[]);
void _parseOutputRedirection(tProcess**, char*);
void _parseInputRedirection(tProcess**, char*);