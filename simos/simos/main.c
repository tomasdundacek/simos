#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "disk.h"
#include "error.h"
#include "runtime.h"
#include "shell.h"
#include "extras.h"

tProg programs[] = {
	{ "md", &md },
	{ "rd", &rd },
	{ "dir", &dir },
	{ "shell", &shell },
	{ "cd", &cd },
	{ "rand", &rand_func },
	{ "type", &type },
	{ "wc", &wc },
	{ "sort", &sort },
	{ "echo", &echo },
	{ "freq", &freq }
};
int programsCount = sizeof(programs) / sizeof(programs[0]);
tList* processes;

int main() {
	tProcess* shellProcess = malloc(sizeof(tProcess));
	shellProcess->programName = malloc((strlen("shell") + 1) * sizeof(char));
	shellProcess->arguments = malloc(sizeof(char));
	strcpy_s(shellProcess->programName, strlen("shell") + 1, "shell");
	shellProcess->arguments = "";
	shellProcess->inputRedirectionFileName = NULL;
	shellProcess->outputRedirectionFileName = NULL;
	
	createList(processes);

	setup();

	callProgram(shellProcess);
	return 0;
}

void setup() {
	if (!initDisk())
		error(ERR_DISK_INIT);
}

void teardown() {
	freeDisk();
}